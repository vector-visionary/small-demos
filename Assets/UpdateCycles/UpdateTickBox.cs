using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.Demos.FloatingPoint
{
    public class UpdateTickBox : MonoBehaviour
    {
        public enum UpdateType { Update, LateUpdate, FixedUpdate, ThreadedTime }
        public UpdateType updateType = UpdateType.Update;

		private void Update()
		{
			if (updateType == UpdateType.Update) UpdateTickBoxManager.Tick(this);
		}
		private void LateUpdate()
		{
			if (updateType == UpdateType.LateUpdate) UpdateTickBoxManager.Tick(this);
		}
		private void FixedUpdate()
		{
			if (updateType == UpdateType.FixedUpdate) UpdateTickBoxManager.Tick(this);
		}
	}

}