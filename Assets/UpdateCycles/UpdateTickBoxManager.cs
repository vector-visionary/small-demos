using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using MantaMiddleware.Demos.ObjectPools;

namespace MantaMiddleware.Demos.FloatingPoint
{
	public class UpdateTickBoxManager : MonoBehaviour
	{
		public static UpdateTickBoxManager Instance;

		public float timeInDisplay = 1f;
		public float timeToExpireTick = 5f;

		private HashSet<UpdateTick> allTicks = new HashSet<UpdateTick>();
		public bool useUnityTime = true;

		public float currentSystemClockTime => (float)(stopwatch.Elapsed.TotalMilliseconds) / 1000f;
		private Stopwatch stopwatch;

		public UpdateTickDisplayer displayerPrefab;
		private ObjectPool<UpdateTickDisplayer> displayerPool;

		private void Awake()
		{
			stopwatch = new Stopwatch();
			stopwatch.Start();
			Instance = this;
			displayerPool = new ObjectPool<UpdateTickDisplayer>(transform, displayerPrefab, (o) => { });
		}

		public static void Tick(UpdateTickBox updateTickBox)
		{
			var newTick = new UpdateTick() { parent = updateTickBox, unityReportedTime = Time.time, systemClockTime = Instance.currentSystemClockTime };
			Instance.allTicks.Add(newTick);
		}

		private void LateUpdate()
		{
			float currentEndTime = useUnityTime ? Time.time : currentSystemClockTime;
			float currentStartTime = currentEndTime - timeInDisplay;
			HashSet<UpdateTick> removingTicks = new HashSet<UpdateTick>();
			displayerPool.ReleaseAll();
			foreach (var tick in allTicks)
			{
				if (tick.systemClockTime < currentEndTime - timeToExpireTick)
				{
					removingTicks.Add(tick);
				}
				else
				{
					float thisTickTime = useUnityTime ? tick.unityReportedTime : tick.systemClockTime;
					if (thisTickTime > currentStartTime)
					{
						var displayer = displayerPool.GetObjectFromPool();
						displayer.Set(tick, (thisTickTime - currentStartTime) / (currentEndTime - currentStartTime));
					}
				}
			}

			foreach (var tick in removingTicks)
			{
				allTicks.Remove(tick);
			}
		}

		
	}

	[System.Serializable]
	public class UpdateTick
	{
		public UpdateTickBox parent;
		public float unityReportedTime;
		public float systemClockTime;
	}
}