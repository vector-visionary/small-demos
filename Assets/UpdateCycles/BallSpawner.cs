using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public GameObject ball;

    public int numBalls = 1;
    public int maxBallsPerFrame = 500;

    private List<GameObject> spawnedBalls = new List<GameObject>();

    void Update()
    {
        int ballsThisFrame = 0;
        while (spawnedBalls.Count < numBalls && ballsThisFrame++ < maxBallsPerFrame) {
            var spawn = Instantiate(ball, transform.position + Random.onUnitSphere, Quaternion.identity);
            spawnedBalls.Add(spawn);
            spawn.GetComponent<Rigidbody>().velocity = Random.onUnitSphere;
        }
    }
}
