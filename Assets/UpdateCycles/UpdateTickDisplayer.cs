using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.Demos.FloatingPoint
{

    public class UpdateTickDisplayer : MonoBehaviour
    {
        public RectTransform rectTransform => (RectTransform)transform;
        private UpdateTick myTick;

        public void Set(UpdateTick tick, float timeNormal)
		{
            myTick = tick;
            rectTransform.SetParent(tick.parent.transform, false);
            rectTransform.anchorMin = new Vector2(timeNormal, rectTransform.anchorMin.y);
            rectTransform.anchorMax = new Vector2(timeNormal, rectTransform.anchorMax.y);
        }
    }
}