﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class FindEquivalents : MonoBehaviour
{
    public TMP_InputField[] eulerInputs;
    public TMP_InputField[] rangeInputs;

    public Transform camTranform;
    private Vector3 camStartPos;

    public Vector3 newInputValue = Vector3.zero;
    private Vector3 lastInputValue = Vector3.zero;
    public Vector2 newRange = new Vector2(0f, 360f);
    private Vector2 oldRange = Vector2.zero;
    public float spacing = 10f;

    public AxisInfoDisplay displayPrefab;
    private List<AxisInfoDisplay> inSceneAxes = new List<AxisInfoDisplay>();
    private Stack<AxisInfoDisplay> unusedSceneAxes = new Stack<AxisInfoDisplay>();

    private float lastUpdate = -99f;

	private void Awake()
	{
        camStartPos = camTranform.position;
	}

	void Update()
    {

        for (int i = 0; i < 3 && i < eulerInputs.Length; i++)
        {
            if (float.TryParse(eulerInputs[i].text, out var val))
            {
                newInputValue[i] = val;
            }
        }
        for (int i = 0; i < 2 && i < rangeInputs.Length; i++)
        {
            if (float.TryParse(rangeInputs[i].text, out var val))
            {
                newRange[i] = val;
            }
        }

        if (Input.GetMouseButton(1))
        {
            newInputValue.y += Input.GetAxis("Mouse X");
            newInputValue.x += Input.GetAxis("Mouse Y");
            for (int i = 0; i < 3 && i < eulerInputs.Length; i++)
            {
                eulerInputs[i].text = newInputValue[i].ToString("#.00");
            }
        }

        if (Time.time > lastUpdate + 0.05f && (newInputValue != lastInputValue || newRange != oldRange ))
        {
            UpdateDisplays(newInputValue, newRange);
            lastInputValue = newInputValue;
            oldRange = newRange;
            lastUpdate = Time.time;
        }

    }

    public void SetCameraPosition(float normValue)
	{
        Vector3 endPos = camStartPos + new Vector3(spacing * inSceneAxes.Count, 0f, 0f);
        camTranform.position = Vector3.Lerp(camStartPos, endPos, normValue);
	}

    public void UpdateDisplays(Vector3 input, Vector2 range)
    {
        foreach (var disp in inSceneAxes)
        {
            unusedSceneAxes.Push(disp);
            disp.gameObject.SetActive(false);
        }
        inSceneAxes.Clear();

        var equivalents = GetEulerEquivalents(input, range.x, range.y);
//        Debug.Log($"Found {equivalents.Length} equivalents to {input}\n{string.Join("\n", equivalents)}");
        for (int e = 0; e < equivalents.Length; e++)
        {
            GameObject go;
            if (unusedSceneAxes.Count > 0)
            {
                go = unusedSceneAxes.Pop().gameObject;
                go.SetActive(true);
            }
            else
            {
                go = Instantiate(displayPrefab.gameObject);
            }
            go.transform.position = new Vector3(spacing * e, 0f, 0f);
            var axis = go.GetComponent<AxisInfoDisplay>();
            axis.euler = equivalents[e];
            inSceneAxes.Add(axis);
        }
    }

    private Vector3[] GetEulerEquivalents(Vector3 input, float lowestBound, float highestBound)
    {
        List<Vector3> rtn = new List<Vector3>();

        for (float xBase = lowestBound; xBase <= highestBound; xBase += 90f)
        {
            for (float yBase = lowestBound; yBase <= highestBound; yBase += 90f)
            {
                for (float zBase = lowestBound; zBase <= highestBound; zBase += 90f)
                {
                    AddIfCongruent(rtn, input, new Vector3(xBase + input.x, yBase + input.y, zBase + input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase + input.x, yBase + input.y, zBase - input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase + input.x, yBase - input.y, zBase + input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase + input.x, yBase - input.y, zBase - input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase - input.x, yBase + input.y, zBase + input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase - input.x, yBase + input.y, zBase - input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase - input.x, yBase - input.y, zBase + input.z));
                    AddIfCongruent(rtn, input, new Vector3(xBase - input.x, yBase - input.y, zBase - input.z));
                }
            }
        }
        return rtn.Distinct().ToArray();
    }

    private void AddIfCongruent(List<Vector3> list, Vector3 original, Vector3 testing)
    {
        Quaternion origQuat = Quaternion.Euler(original);
        Quaternion testQuat = Quaternion.Euler(testing);
        Vector3[] testTargetVectors = new Vector3[] { Vector3.forward, Vector3.up, Vector3.right };
        foreach (var tv in testTargetVectors)
        {
            if ((origQuat * tv - testQuat * tv).sqrMagnitude > 0.00001f) return;
        }
        list.Add(testing);
    }
}
