using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MouseLookWatcher : MonoBehaviour
{
    public TMP_Text text;
    public MouseLook watching;

    public int step = 0;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) step++;
        if (step == 0)
        {
            watching.rotateAxesIndividually = false;
            watching.doClampRotation = false;
            watching.doTrackRotation = false;
            text.text = $"Using Transform.Rotate()";
        }
        else if (step == 1)
        {
            watching.rotateAxesIndividually = true;
            watching.doClampRotation = false;
            watching.doTrackRotation = false;
            text.text = $"Using Transform.Rotate() with different axes\ntransform.rotation.x = {watching.transform.rotation.x}";
        }
        else if (step == 2)
        {
            watching.rotateAxesIndividually = true;
            watching.doClampRotation = true;
            watching.doTrackRotation = false;
            text.text = $"Using Transform.Rotate() with clamping\ntransform.eulerAngles.x = {watching.transform.eulerAngles.x}";
        }
        else if (step == 3)
        {
            watching.doClampRotation = false;
            watching.doTrackRotation = true;
            text.text = $"Tracking our own rotation\nwatching.trackedRotation.x = {watching.trackedRotation.x}";
        }
        else if (step == 4)
        {
            watching.doClampRotation = true;
            watching.doTrackRotation = true;
            text.text = $"Tracking and clamping our own rotation\nwatching.trackedRotation.x = {watching.trackedRotation.x}";
        }



    }
}
