using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class WalkerUI : MonoBehaviour
{
    public TMP_Text output;

	private Walker target;

	public void SetTarget(Walker walker)
	{
		var walkerCam = walker.GetComponentInChildren<Camera>();
		RectTransform rt = GetComponent<RectTransform>();
		rt.anchorMin = new Vector2(walkerCam.rect.xMin, walkerCam.rect.yMin);
		rt.anchorMax = new Vector2(walkerCam.rect.xMax, walkerCam.rect.yMax);
		target = walker;
	}

	private void Update()
	{
		if (target != null)
		{
			output.text = $"Spd: {target.moveSpeed}\nMoved:{target.totalMovement.x:#.0}\nTurned:{target.totalMovement.y:#.0} degrees";
		}
		else
		{
			output.text = "";
		}
	}
}
