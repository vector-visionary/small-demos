using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ThirdPersonCamera : MonoBehaviour
{
	private Camera m_camera;
	public new Camera camera => m_camera;

	[SerializeField] private Transform target;
	[SerializeField] private Vector3 followPosition = new Vector3(0f, 1.5f, 2f);

	private void Awake()
	{
		m_camera = GetComponentInChildren<Camera>();
	}

	private void Update()
	{
		if (target != null)
		{
			transform.position = target.position;
			Vector3 charForward = target.forward;
			charForward.y = 0f;
			transform.rotation = Quaternion.LookRotation(charForward);
			camera.transform.localPosition = followPosition;
		}
	}

	public Vector3 GetMovementVector(Vector2 input, bool flatten)
	{
		var rtn = transform.TransformDirection(new Vector3(input.x, 0f, input.y));
		if (flatten) rtn.y = 0f;
		return rtn.normalized;
	}
}
