using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

namespace MantaMiddleware.Demos.FloatingPoint
{
    public class PositionDisplayer : MonoBehaviour
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] private TMP_Text outputField;

		private void Update()
		{
			outputField.text = $"{targetTransform.gameObject.name}: Position is {targetTransform.position}";

			// outputField.text = $"{targetTransform.gameObject.name}: Position is {targetTransform.position:#.0000}";
		}

		[ContextMenu("Test")]
		public void Test()
		{

			for (int x=0; x<100; x++)
			{
				float dec = (float)x / 100f;
				for (int whole = 0; whole < 10; whole++)
				{
					float combined = whole * dec;
					if (combined.ToString().Length > 5)
					{
						Debug.Log($"<b>Floating point math check</b>: {dec} * {whole} result: {combined}\n\n");
					}
				}
			}
		}
	}
}