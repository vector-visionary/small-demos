using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace MantaMiddleware.Demos.FloatingPoint
{
    public class FailureDetector : MonoBehaviour
    {
        [SerializeField] private TMP_Text timeOutput;
        [SerializeField] private GameObject failureDisplay;
		[SerializeField] private float timeLimit = 15f;
		private void Update()
		{
			timeOutput.text = $"Current time is {Time.time:#.00} out of {timeLimit:#.00}";


			if (Time.time == timeLimit)
			//if (Time.time > timeLimit - 0.2f && Time.time < timeLimit + 0.2f)
			//if (Mathf.Abs(Time.time - timeLimit) < 0.01f)
			//if (Time.time > timeLimit)
			{
				failureDisplay.SetActive(true);
			}
			if (Time.time > timeLimit - 0.2f && Time.time < timeLimit + 0.2f) Debug.Log($"Time was {Time.time}");
		}
	}
}