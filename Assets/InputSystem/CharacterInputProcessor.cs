using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MantaMiddleware.Demos.InputSystem
{
	public class CharacterInputProcessor : MonoBehaviour
	{
		[SerializeField] private ThirdPersonCamera camera;
		[SerializeField] private float speed = 5f;
		[SerializeField] private float slowSpeed = 0.5f;
		[SerializeField] private float rotSpeed = 0.1f;

		public void InputChanged(PlayerInput playerInput)
		{
			if (playerInput.inputIsActive)
			{
				foreach (var map in playerInput.actions.actionMaps)
				{
					Debug.Log($"Found map {map.name}");
				}
			}
		}

		public void Move(InputAction.CallbackContext context)
		{
			movement = context.ReadValue<Vector2>();
		}
		public void SetSlowness(InputAction.CallbackContext context)
		{
			slowness = context.ReadValue<float>();
		}

		private Vector2 movement = Vector2.zero;
		private Vector2 rotationValues = Vector2.zero;
		private float slowness = 0f;

		private void Awake()
		{
			
		}
		private void Update()
		{
			Vector3 motionInSpace = camera.GetMovementVector(movement, true);
			transform.position += motionInSpace * Mathf.Lerp(speed, slowSpeed, slowness) * Time.deltaTime;
			rotationValues.x += Mouse.current.delta.ReadValue().x * rotSpeed;
			transform.rotation = Quaternion.Euler(0f, rotationValues.x, 0f);
		}
	}
}